# Checkout kata

## Requirements

Implement the code for a supermarket checkout that calculates the total price of a number of
items. In a normal supermarket, things are identified using Stock Keeping Units, or SKUs. In our
store, we’ll use individual letters of the alphabet (A, B, C, and so on as the SKUs). Our goods
are priced individually. In addition, some items are multi-priced: buy n of them, and they’ll cost
you y. For example, item ‘A’ might cost 50 pence individually, but this week we have a special
offer: buy three ‘A’s and they’ll cost you £1.30. In fact this week’s prices are:

| Item | Unit Price | Special Price |
|------|------------|---------------|
| A | 50 | 3 for 130 |
| B | 30 | 2 for 45 |
| C | 20 | |
| D | 15 | |

Our checkout accepts items in any order, so that if we scan a B, an A, and another B, we’ll
recognise the two B’s and price them at 45 (for a total price so far of 95). Because the pricing
changes frequently, we need to be able to pass in a set of pricing rules each time we start
handling a checkout transaction.

## Implementation

A TDD approach was followed, implementing the basic single price handling first, then extracting out a rule interface
to enable further discount rules to be applied. A single list of rules, with single price and discounts handled the 
same way was selected for simplicity and enable easy extensibility.

The build and dependencies are managed using Maven

To execute the tests simply run `mvn test`

## Further enhancements

### Verification of single price rules
At moment it is possible to add any number of single price rules for an SKU which will all be applied.
If this is not desired then introducing a PriceRuleEngine for creation and management of rules might be an option.
This could ensure there is only 1 single price rule per SKU while allowing any number of other rules

### Handle invalid SKU's
Entering a non existent or empty SKU does not have any affect on the total, they are just silently ignored.
It may be better to detect unknown SKU's and throw an exception to indicate invalid, giving the client the chance to
correct the value or warn.

### Other discount rules
It is possible to extend the code to handle other potential discounts if required. For example a 50% off rule could
take the single price rule as input and simply return a discount amount of half the price.
