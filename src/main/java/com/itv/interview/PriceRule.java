package com.itv.interview;

import java.util.List;

/**
 * Defines a price rule to be applied to matching items by SKU
 */
public interface PriceRule {
    /**
     * Determines if the rule applies to an item.
     * @param sku The items SKU
     * @param skus The SKUs of pre scanned items
     * @return true if rule has been met, otherwise false
     */
    boolean appliesTo(String sku, List<String> skus);

    /**
     * Returns the amount to apply to the total if the rule applies.
     * For discounts this amount should be negative
     * @return The amount to apply to the total
     */
    long getAmount();
}
