package com.itv.interview;

import java.util.List;

/**
 * Defines rule for applying MultiBuy discount
 */
public class MultiBuyPriceRule implements PriceRule {

    private final String sku;
    private final String description;
    private final long itemsRequired;
    private final long amount;

    /**
     * @param sku SKU of item rule applies to
     * @param description Description of offer
     * @param itemsRequired Number of items required for discount to be applied
     * @param amount The discount amount specified as negative number to apply to total.
     */
    public MultiBuyPriceRule(String sku, String description, long itemsRequired, long amount) {
        this.sku = sku;
        this.description = description;
        this.itemsRequired = itemsRequired;
        this.amount = amount;
    }

    @Override
    public boolean appliesTo(String sku, List<String> scannedSkus) {
        if(this.sku.equals(sku)) {
            long itemsBought = scannedSkus.stream().filter(listSku -> this.sku.equals(listSku)).count();
            return itemsBought % itemsRequired == 0;
        }
        return false;
    }

    @Override
    public long getAmount() {
        return amount;
    }
}
