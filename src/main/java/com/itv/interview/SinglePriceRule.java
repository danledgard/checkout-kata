package com.itv.interview;

import java.util.List;

/**
 * Defines rule for price to apply to individually scanned items
 */
public class SinglePriceRule implements PriceRule {
    private final String sku;
    private final long amount;

    /**
     * @param sku SKU of item rule applies to
     * @param amount Price of item
     */
    public SinglePriceRule(String sku, long amount) {
        this.sku = sku;
        this.amount = amount;
    }

    @Override
    public boolean appliesTo(String sku, List<String> skus) {
        return this.sku.equals(sku);
    }

    @Override
    public long getAmount() {
        return amount;
    }
}
