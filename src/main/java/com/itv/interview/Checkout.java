package com.itv.interview;

import java.util.ArrayList;
import java.util.List;

public class Checkout {

    final private List<PriceRule> rules = new ArrayList<>();
    private long total = 0;
    final private List<String> scannedSkus = new ArrayList<>();

    public Checkout(List<PriceRule> rules) {
        this.rules.addAll(rules);
    }

    /**
     * The current total of scanned items
     * @return total value
     */
    public long getTotal() {
        return total;
    }

    /**
     * Scan an item and apply price and any discounts to total
     * @param sku SKU of item scanned
     */
    public void scan(String sku) {
        scannedSkus.add(sku);

        rules.stream().filter(rule -> rule.appliesTo(sku, scannedSkus)).forEach(rule -> {
            this.total += rule.getAmount();
        });
    }
}
