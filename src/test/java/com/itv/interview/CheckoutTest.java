package com.itv.interview;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class CheckoutTest {

    private Checkout subject;

    @Test
    public void shouldInitialiseTotalToZero() {

        subject = new Checkout(new ArrayList<>());

        assertThat(subject.getTotal()).isEqualTo(0);
    }

    @Test
    public void shouldIncrementTotalByPriceOfScannedItem() {

        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));

        subject = new Checkout(rules);
        subject.scan("A");

        assertThat(subject.getTotal()).isEqualTo(50);
    }

    @Test
    public void shouldTotalMultipleScannedItemsCorrectly() {

        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("A");

        assertThat(subject.getTotal()).isEqualTo(100);
    }

    @Test
    public void shouldTotalDifferentScannedItemsCorrectly() {

        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));
        rules.add(new SinglePriceRule("B", 30));
        rules.add(new SinglePriceRule("C", 20));
        rules.add(new SinglePriceRule("D", 15));

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("B");

        assertThat(subject.getTotal()).isEqualTo(80);

        subject = new Checkout(rules);
        subject.scan("C");
        subject.scan("D");

        assertThat(subject.getTotal()).isEqualTo(35);

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("B");
        subject.scan("C");
        subject.scan("D");

        assertThat(subject.getTotal()).isEqualTo(115);
    }

    @Test
    public void shouldApplyMultiBuyDiscountWhenCorrectNumberOfItemsScanned() {

        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));
        rules.add(new MultiBuyPriceRule("A","3 for 130", 3,-20));

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");

        assertThat(subject.getTotal()).isEqualTo(130);
    }

    @Test
    void shouldOnlyApplyMultiBuyDiscountForQualifyingGroupsOfItems() {
        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));
        rules.add(new MultiBuyPriceRule("A","3 for 130", 3,-20));

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");

        assertThat(subject.getTotal()).isEqualTo(230);
    }

    @Test
    void shouldApplyMultiBuyDiscountForAllQualifyingGroupsOfItems() {
        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));
        rules.add(new MultiBuyPriceRule("A","3 for 130", 3,-20));

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");
        subject.scan("A");

        assertThat(subject.getTotal()).isEqualTo(260);
    }

    @Test
    void shouldApplyMultiBuyDiscountWhenItemsScannedInAnyOrder() {
        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));
        rules.add(new MultiBuyPriceRule("A","3 for 130", 3,-20));
        rules.add(new SinglePriceRule("B", 30));
        rules.add(new SinglePriceRule("C", 20));

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("B");
        subject.scan("A");
        subject.scan("C");
        subject.scan("A");

        assertThat(subject.getTotal()).isEqualTo(180);
    }

    @Test
    void shouldApplyMultiBuyDiscountsForAllQualifyingItems() {
        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));
        rules.add(new MultiBuyPriceRule("A","3 for 130", 3,-20));
        rules.add(new SinglePriceRule("B", 30));
        rules.add(new MultiBuyPriceRule("B","2 for 45", 2,-15));

        subject = new Checkout(rules);
        subject.scan("A");
        subject.scan("B");
        subject.scan("A");
        subject.scan("A");
        subject.scan("B");

        assertThat(subject.getTotal()).isEqualTo(175);
    }

    @Test
    void shouldIncrementallyTotalAmountAsItemsScanned() {
        List<PriceRule> rules = new ArrayList<>();
        rules.add(new SinglePriceRule("A", 50));
        rules.add(new MultiBuyPriceRule("A","3 for 130", 3,-20));
        rules.add(new SinglePriceRule("B", 30));
        rules.add(new MultiBuyPriceRule("B","2 for 45", 2,-15));
        rules.add(new SinglePriceRule("C", 20));
        rules.add(new SinglePriceRule("D", 15));

        subject = new Checkout(rules);

        subject.scan("A");
        assertThat(subject.getTotal()).isEqualTo(50);
        subject.scan("B");
        assertThat(subject.getTotal()).isEqualTo(80);
        subject.scan("C");
        assertThat(subject.getTotal()).isEqualTo(100);
        subject.scan("D");
        assertThat(subject.getTotal()).isEqualTo(115);
        subject.scan("A");
        assertThat(subject.getTotal()).isEqualTo(165);
        subject.scan("B");
        assertThat(subject.getTotal()).isEqualTo(180);
        subject.scan("A");
        assertThat(subject.getTotal()).isEqualTo(210);
    }
}
